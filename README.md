# Cimri Challenge Application #

This README file prepared for Cimri Challenge.


### Technologies ###
* Spring Boot
* MySQL
* Cassandra
* Java 8
* Thymleaf
* Angular

### Step by Step for Run Application ###

* First of all, in your local you must have `MySQL`, `Cassandra` and `Java 8`
* Clone the repository your local
* Execute `mvn clean install`
* You must give `Database Name`, `User name` and `Password` in application.properties
* You must run `setup.cql` file(It is in resources folder)
* After you can execute `mvn spring-boot:run` command
* If you not change `context-path` and `port` in `application.properties` file, you can access with this url `http://localhost:8080/challenge-app`

## NOTE ##
Every run the application, all data will be removed and crawling data will be started again.