package com.cimri.challenge.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * It parse xml data to requested class object.
 */
public class XmlParser {
    private final Logger logger = LoggerFactory.getLogger(XmlParser.class);

    private static ObjectMapper mapper = new XmlMapper();

    public <T> T parse(String content, Class<T> clazz) {
        try {
            return mapper.readValue(content, clazz);
        } catch (IOException e) {
            logger.error("XML Parsing error {}", e.getMessage());
        }
        return null;
    }
}
