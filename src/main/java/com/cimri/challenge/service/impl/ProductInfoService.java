package com.cimri.challenge.service.impl;

import com.cimri.challenge.entity.db.Product;
import com.cimri.challenge.entity.db.ProductInfo;
import com.cimri.challenge.repository.db.ProductInfoRepository;
import com.cimri.challenge.service.IProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductInfoService implements IProductInfoService {

    @Autowired
    ProductInfoRepository productInfoRepository;

    @Override
    public ProductInfo save(ProductInfo productInfo) {
        return productInfoRepository.save(productInfo);
    }

    @Override
    public ProductInfo findBySourceAndProduct(String source, Product product) {
        return productInfoRepository.findBySourceAndProduct(source, product);
    }

    @Override
    public List<ProductInfo> findByProduct(Product product) {
        return productInfoRepository.findByProduct(product);
    }
}
