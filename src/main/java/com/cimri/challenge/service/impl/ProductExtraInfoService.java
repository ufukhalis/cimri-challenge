package com.cimri.challenge.service.impl;

import com.cimri.challenge.entity.cassandra.ProductExtraInfo;
import com.cimri.challenge.repository.cassandra.ProductExtraInfoRepository;
import com.cimri.challenge.service.IProductExtraInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductExtraInfoService implements IProductExtraInfoService {

    @Autowired
    ProductExtraInfoRepository productExtraInfoRepository;

    @Override
    public ProductExtraInfo save(ProductExtraInfo productExtraInfo) {
        return productExtraInfoRepository.save(productExtraInfo);
    }

    @Override
    public List<ProductExtraInfo> findByProductInfoId(long productInfoId) {
        return productExtraInfoRepository.findByProductInfoId(productInfoId);
    }
}
