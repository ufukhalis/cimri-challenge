package com.cimri.challenge.service.impl;

import com.cimri.challenge.entity.db.Product;
import com.cimri.challenge.repository.db.ProductRepository;
import com.cimri.challenge.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService implements IProductService{

    @Autowired
    ProductRepository productRepository;

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product findByTitle(String title) {
        return productRepository.findByTitle(title);
    }

    @Override
    public Iterable<Product> saveAll(Iterable<Product> products) {
        return productRepository.save(products);
    }

    @Override
    public List<Product> findByTitleContaining(String title) {
        return productRepository.findByTitleContaining(title);
    }

    @Override
    public Page<Product> findByTitleContaining(String title, int start, int size) {
        return productRepository.findByTitleContaining(new PageRequest(start, size), title);
    }

    @Override
    public Product findById(long id) {
        return productRepository.findOne(id);
    }

}
