package com.cimri.challenge.service;

import com.cimri.challenge.entity.db.Product;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IProductService {
    Product save(Product product);
    Product findByTitle(String title);
    Iterable<Product> saveAll(Iterable<Product> products);
    List<Product> findByTitleContaining(String title);
    Page<Product> findByTitleContaining(String title, int start, int size);
    Product findById(long id);
}
