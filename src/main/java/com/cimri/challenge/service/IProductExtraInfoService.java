package com.cimri.challenge.service;

import com.cimri.challenge.entity.cassandra.ProductExtraInfo;

import java.util.List;

public interface IProductExtraInfoService {
    ProductExtraInfo save(ProductExtraInfo productExtraInfo);

    List<ProductExtraInfo> findByProductInfoId(long productInfoId);
}
