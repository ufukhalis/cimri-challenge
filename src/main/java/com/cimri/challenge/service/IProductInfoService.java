package com.cimri.challenge.service;

import com.cimri.challenge.entity.db.Product;
import com.cimri.challenge.entity.db.ProductInfo;

import java.util.List;

public interface IProductInfoService {
    ProductInfo save(ProductInfo productInfo);

    ProductInfo findBySourceAndProduct(String source, Product product);

    List<ProductInfo> findByProduct(Product product);
}
