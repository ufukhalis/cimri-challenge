package com.cimri.challenge.http;

import com.cimri.challenge.xml.XmlModel;
import com.cimri.challenge.xml.XmlParser;
import com.github.kevinsawicki.http.HttpRequest;

import java.util.Arrays;

/**
 * It requests given url and converts Response object
 */
public class Request {
    public Response get(String url, String source) {
        return new Response(
                Arrays.asList(new XmlParser().parse(HttpRequest.get(url).body("UTF-8"), XmlModel[].class)),
                source
        );
    }
}
