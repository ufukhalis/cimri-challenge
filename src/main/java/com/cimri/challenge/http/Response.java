package com.cimri.challenge.http;

import com.cimri.challenge.xml.XmlModel;

import java.util.List;

/**
 * Http response object model
 */
public class Response {
    private String source;
    private List<XmlModel> models;

    public Response(List<XmlModel> models) {
        this.models = models;
    }

    public Response(List<XmlModel> models, String source) {
        this.models = models;
        this.source = source;
    }

    public void setModels(List<XmlModel> models) {
        this.models = models;
    }

    public List<XmlModel> getModels() {
        return models;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }
}
