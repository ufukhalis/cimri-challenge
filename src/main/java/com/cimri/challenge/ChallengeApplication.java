package com.cimri.challenge;

import com.cimri.challenge.crawler.AsyncCrawlerExecutor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ChallengeApplication {

	private static ApplicationContext CONTEXT = null;

	public static void main(String[] args) {
		setContext(SpringApplication.run(ChallengeApplication.class, args));
		new AsyncCrawlerExecutor().start();
	}

	private static void setContext(ApplicationContext CONTEXT) {
		ChallengeApplication.CONTEXT = CONTEXT;
	}

	public static <T> T getBean(Class<T> clazz) {
		return CONTEXT.getBean(clazz);
	}
}
