package com.cimri.challenge.repository.db;

import com.cimri.challenge.entity.db.Product;
import com.cimri.challenge.entity.db.ProductInfo;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface ProductInfoRepository extends BaseRepository<ProductInfo, Long> {

    ProductInfo findBySourceAndProduct(String source, Product product);

    List<ProductInfo> findByProduct(Product product);
}