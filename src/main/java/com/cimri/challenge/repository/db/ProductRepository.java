package com.cimri.challenge.repository.db;

import com.cimri.challenge.entity.db.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface ProductRepository extends BaseRepository<Product, Long> {

    Product findByTitle(String title);
    List<Product> findByTitleContaining(String title);
    Page<Product> findByTitleContaining(Pageable pageable, String title);
}