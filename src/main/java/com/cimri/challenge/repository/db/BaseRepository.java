package com.cimri.challenge.repository.db;

import com.cimri.challenge.entity.db.BaseEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;

@NoRepositoryBean
public interface BaseRepository<E extends BaseEntity, S extends Serializable> extends PagingAndSortingRepository<E, S> {

}