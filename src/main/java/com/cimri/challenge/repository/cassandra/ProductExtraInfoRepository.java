package com.cimri.challenge.repository.cassandra;

import com.cimri.challenge.entity.cassandra.ProductExtraInfo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface ProductExtraInfoRepository extends CrudRepository<ProductExtraInfo, UUID> {

    List<ProductExtraInfo> findByProductInfoId(long productInfoId);
}
