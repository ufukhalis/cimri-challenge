package com.cimri.challenge.controller;


import com.cimri.challenge.dto.GraphicDTO;
import com.cimri.challenge.entity.cassandra.ProductExtraInfo;
import com.cimri.challenge.service.IProductExtraInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class GraphicController {

    @Autowired
    IProductExtraInfoService productExtraInfoService;

    @RequestMapping(value="/graphic", method = RequestMethod.GET)
    public String displayGraphicPage(Model model, @RequestParam(name = "id") long id) {
        List<ProductExtraInfo> extras = productExtraInfoService.findByProductInfoId(id);
        model.addAttribute("extraData", new GraphicDTO().convert(extras));
        return "views/graphic";
    }
}
