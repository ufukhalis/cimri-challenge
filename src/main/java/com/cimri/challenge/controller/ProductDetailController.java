package com.cimri.challenge.controller;


import com.cimri.challenge.entity.db.Product;
import com.cimri.challenge.service.IProductInfoService;
import com.cimri.challenge.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProductDetailController {

    @Autowired
    IProductInfoService productInfoService;
    @Autowired
    IProductService productService;

    @RequestMapping(value="/product-detail", method = RequestMethod.GET)
    public String displayProductDetailPage(Model model, @RequestParam(name = "id") long id) {
        Product product = productService.findById(id);
        model.addAttribute("product", product);
        model.addAttribute("infoList", productInfoService.findByProduct(product));
        return "views/product-detail";
    }
}
