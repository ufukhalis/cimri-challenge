package com.cimri.challenge.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

/**
 * Basic Cassandra configurations
 * It enables repositories for Cassandra
 */
@Configuration
@EnableCassandraRepositories(basePackages = "com.cimri.challenge.repository.cassandra")
public class CasssandraConfig {

}
