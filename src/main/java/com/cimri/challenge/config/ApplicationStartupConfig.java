package com.cimri.challenge.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.stereotype.Component;

/**
 * It runs when application startup and truncate Cassandra tables.
 */
@Component
public class ApplicationStartupConfig implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    private Environment environment;

    @Autowired
    private CassandraOperations cassandraTemplate;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        cassandraTemplate.truncate(environment.getProperty("cassandra.table.product-extra-info"));
    }
}