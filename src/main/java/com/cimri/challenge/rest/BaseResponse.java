package com.cimri.challenge.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.Serializable;

public class BaseResponse<T> implements Serializable {

    private int statusCode;
    private String message;
    private transient T result;

    private BaseResponse(Builder<T> builder) {
        this.message = builder.error;
        this.statusCode = builder.statusCode;
        this.result = builder.result;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public static class Builder<T> {
        private int statusCode;
        private String error;
        private T result;

        public Builder(T result) {
            this.result = result;
        }

        public Builder message(String message) {
            this.error = message;
            return this;
        }

        public Builder statusCode(int statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public BaseResponse build() {
            return new BaseResponse<>(this);
        }

    }

    public static <T> ResponseEntity<BaseResponse<T>> responseEntity(T result, String message, HttpStatus httpStatus) {
        BaseResponse response = new BaseResponse.Builder<>(result).message(message).statusCode(httpStatus.value()).build();
        return new ResponseEntity<>(response, httpStatus);
    }
}