package com.cimri.challenge.rest;

import com.cimri.challenge.dto.GraphicDTO;
import com.cimri.challenge.entity.cassandra.ProductExtraInfo;
import com.cimri.challenge.entity.db.Product;
import com.cimri.challenge.service.IProductExtraInfoService;
import com.cimri.challenge.service.IProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductRestController {

    private final Logger logger = LoggerFactory.getLogger(ProductRestController.class);

    @Autowired
    IProductService productService;

    @Autowired
    IProductExtraInfoService productExtraInfoService;

    @RequestMapping(value = "/find-products-by-title", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse<Page<Product>>> findProductsByTitle(
            @RequestParam(value = "title") String title,
            @RequestParam(value = "start", defaultValue = "0") int start,
            @RequestParam(value = "size", defaultValue = "10") int size) {
        try {
            Page<Product> products = productService.findByTitleContaining(title, start, size);
            return BaseResponse.responseEntity(products, null, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Server couldn't give response {}", e.getMessage());
            return BaseResponse.responseEntity(null, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/find-extras-by-id", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse<List<GraphicDTO>>> findExtrasByInfoId(
            @RequestParam(value = "id") long id) {
        try {
            List<ProductExtraInfo> extras = productExtraInfoService.findByProductInfoId(id);
            return BaseResponse.responseEntity(new GraphicDTO().convert(extras), null, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Server couldn't give response {}", e.getMessage());
            return BaseResponse.responseEntity(null, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
