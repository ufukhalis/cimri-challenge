package com.cimri.challenge.crawler;

import com.cimri.challenge.http.Request;
import com.cimri.challenge.http.Response;
import com.cimri.challenge.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.stream.Collectors.toList;

/**
 * Non-Blocking async executor.
 * This class crawl xml data from given links.
 * After crawling data, saving process will be start
 */
public class AsyncCrawlerExecutor {

    private final Logger logger = LoggerFactory.getLogger(AsyncCrawlerExecutor.class);

    private final static List<String> URLS = Arrays.asList(
            "https://bitbucket.org/cimri/challenge/raw/3e019ced5866fa09ba63d8c08cb563abc976c0c8/site1.xml",
            "https://bitbucket.org/cimri/challenge/raw/3e019ced5866fa09ba63d8c08cb563abc976c0c8/site2.xml",
            "https://bitbucket.org/cimri/challenge/raw/3e019ced5866fa09ba63d8c08cb563abc976c0c8/site3.xml",
            "https://bitbucket.org/cimri/challenge/raw/3e019ced5866fa09ba63d8c08cb563abc976c0c8/site4.xml"
    );
    private final Request httpRequest;
    private final ExecutorService executorService = Executors.newFixedThreadPool(URLS.size());

    public AsyncCrawlerExecutor(){
        this.httpRequest = new Request();
    }

    public void start() {
        long startTime = System.nanoTime();
        logger.info("XML Crawling started");
        List<CompletableFuture<Response>> requests = URLS.stream()
                .map(
                        url -> CompletableFuture.supplyAsync(() ->
                                        httpRequest.get(
                                                url,
                                                StringUtils.substr(url,"site",".xml"
                                        )),
                                executorService)
                )
                .collect(toList());

        List<Response> responses = requests.stream()
                .map(CompletableFuture::join)
                .collect(toList());

        long endTime = (System.nanoTime() - startTime) / 1_000_000;
        logger.info("XML Crawler finished, total time : {} ms", endTime);
        executorService.shutdown();
        new CrawlerReader().saveCrawlerData(responses);
    }
}
