package com.cimri.challenge.crawler;

import com.cimri.challenge.ChallengeApplication;
import com.cimri.challenge.entity.cassandra.ProductExtraInfo;
import com.cimri.challenge.entity.db.Product;
import com.cimri.challenge.entity.db.ProductInfo;
import com.cimri.challenge.http.Response;
import com.cimri.challenge.service.IProductExtraInfoService;
import com.cimri.challenge.service.IProductInfoService;
import com.cimri.challenge.service.IProductService;
import com.cimri.challenge.utils.StringUtils;
import com.cimri.challenge.xml.XmlModel;
import com.datastax.driver.core.utils.UUIDs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class saves crawled xml products data with using Spring async.
 *
 */
public class CrawlerReader {
    private final Logger logger = LoggerFactory.getLogger(CrawlerReader.class);

    private final IProductService productService = ChallengeApplication.getBean(IProductService.class);
    private final IProductInfoService productInfoService = ChallengeApplication.getBean(IProductInfoService.class);
    private final IProductExtraInfoService productExtraInfoService =
            ChallengeApplication.getBean(IProductExtraInfoService.class);

    @Async
    public void saveCrawlerData(List<Response> responseList) {
        try {
            responseList.forEach(response -> response.getModels().forEach(xmlModel -> {
                Product product = this.productService.findByTitle(xmlModel.getTitle());
                if (product == null) {
                    product = new Product();
                }
                product.setTitle(xmlModel.getTitle());
                product.setBrand(xmlModel.getBrand());
                product = this.productService.save(product);
                if (product != null) {
                    logger.info("Product saved, id {}, title {}", product.getId(), product.getTitle());
                    saveInfo(response, product, xmlModel);
                }
            }));
        } catch (Exception e) {
            logger.error("Product save problem {}", e.getMessage());
            logger.error("Product saving couldn't complete");
        }
    }

    /**
     * This method saves to Cassandra
     * @param dates
     * @param prices
     * @param id
     */
    private void saveExtraInfo(String dates, String prices, long id) {
        ProductExtraInfo info = new ProductExtraInfo();

        List<Long> dateList = StringUtils.stringToList(dates, ",")
                .stream().map(Long::parseLong).collect(Collectors.toList());
        List<BigDecimal> priceList = StringUtils.stringToList(prices, ",")
                .stream().map(BigDecimal::new).collect(Collectors.toList());
        try {
            for (int i=0; i<dateList.size(); i++) {
                info.setId(UUIDs.timeBased());
                info.setProductDate(dateList.get(i));
                info.setProductPrice(priceList.get(i));
                info.setProductInfoId(id);
                this.productExtraInfoService.save(info);
            }
        } catch (Exception e) {
            logger.error("Date or price index error {}", e.getMessage());
        } finally {
            info.setProductInfoId(id);
            this.productExtraInfoService.save(info);
        }
    }

    private void saveInfo(Response response, Product product, XmlModel xmlModel) {
        ProductInfo info = this.productInfoService.findBySourceAndProduct(response.getSource(), product);
        if (info == null) {
            info = new ProductInfo();
            info.setCategory(xmlModel.getCategory());
            info.setProductId(xmlModel.getId());
            info.setUrl(xmlModel.getUrl());
            info.setProduct(product);
            info.setSource(response.getSource());
            info = this.productInfoService.save(info);
            saveExtraInfo(xmlModel.getDates(), xmlModel.getPrices(), info.getId());
        }
    }
}
