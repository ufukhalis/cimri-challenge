package com.cimri.challenge.utils;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Util functions for string objects
 */
public final class StringUtils {

    public static String substr(String text, String firstIndexOf, String lastIndexOf) {
        return text.substring(text.indexOf(firstIndexOf), text.lastIndexOf(lastIndexOf));
    }

    public static List<String> stringToList(String str, String delimeter) {
        if (str == null || str.length() == 0) {
            return new ArrayList<>();
        }
        return Arrays.asList(str.split(delimeter));
    }
}
