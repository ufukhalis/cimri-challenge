package com.cimri.challenge.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Util functions for Date
 */
public final class DateUtils {
    public final static SimpleDateFormat YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd");

    public static String formatDate(Date date, SimpleDateFormat sdf) {
        return sdf.format(date);
    }
}
