package com.cimri.challenge.entity.db;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

import javax.persistence.*;
import java.util.List;

@JsonIgnoreType
@Entity
public class Product extends BaseEntity {

    @Lob
    private String title;
    @Column(length = 128)
    private String brand;
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<ProductInfo> productInfoList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public List<ProductInfo> getProductInfoList() {
        return productInfoList;
    }

    public void setProductInfoList(List<ProductInfo> productInfoList) {
        this.productInfoList = productInfoList;
    }
}
