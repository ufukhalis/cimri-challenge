package com.cimri.challenge.entity.db;

import javax.persistence.*;

@Entity
public class ProductInfo extends BaseEntity {

    private long productId;
    @Lob
    private String url;
    @Column(length = 128)
    private String category;
    @Column(length = 32)
    private String source;
    @ManyToOne
    @JoinColumn(name = "product")
    private Product product;

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }
}
