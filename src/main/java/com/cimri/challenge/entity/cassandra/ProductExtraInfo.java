package com.cimri.challenge.entity.cassandra;

import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import java.math.BigDecimal;
import java.util.UUID;

@Table("product_extra_info")
public class ProductExtraInfo {

    @PrimaryKey
    private UUID id;
    private Long productDate;
    private BigDecimal productPrice;

    private long productInfoId;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getProductDate() {
        return productDate;
    }

    public void setProductDate(Long productDate) {
        this.productDate = productDate;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public long getProductInfoId() {
        return productInfoId;
    }

    public void setProductInfoId(long productInfoId) {
        this.productInfoId = productInfoId;
    }
}
