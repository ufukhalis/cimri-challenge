package com.cimri.challenge.dto;

import com.cimri.challenge.entity.cassandra.ProductExtraInfo;
import com.cimri.challenge.utils.DateUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * DTO class for Date based prices changes to show in a chart
 */
public class GraphicDTO implements Serializable{

    private BigDecimal value;
    private String date;

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getDate() {
        return date;
    }

    /**
     * This method convert our Cassandra table to AmCharts Chart data
     * @param extras
     * @return
     */
    public List<GraphicDTO> convert(List<ProductExtraInfo> extras) {
        extras.sort(Comparator.comparingLong(ProductExtraInfo::getProductDate));
        Function<ProductExtraInfo, GraphicDTO> conversion = productExtraInfo -> {
            GraphicDTO dto = new GraphicDTO();
            dto.setValue(productExtraInfo.getProductPrice());
            dto.setDate(DateUtils.formatDate(new Date(productExtraInfo.getProductDate()), DateUtils.YYYY_MM_DD));
            return dto;
        };
        return extras.stream().map(conversion).collect(Collectors.toList());
    }
}
