function appController($scope, $http) {

    $scope.Init = function () {

    }

    $scope.ButtonClick = function (title) {
        $scope.getProducts(title);
        $('html, body').animate({ scrollTop: 500}, 1000);
    }

    $scope.getProducts = function (title) {
        var url = "api/find-products-by-title?title="+title;
        $http.get(url).success( function(response) {
            $scope.products = response;
        });
    };
}