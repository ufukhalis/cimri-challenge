package com.cimri.challenge;

import com.cimri.challenge.utils.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class UtilTests {
    @Test
    public void substrGivenString() {
        String str = StringUtils.substr(
                "https://bitbucket.org/cimri/challenge/raw/3e019ced5866fa09ba63d8c08cb563abc976c0c8/site1.xml",
                "site",
                ".xml"
        );
        Assert.assertEquals("site1", str);
    }

    @Test
    public void stringToList() {
        List<String> list = StringUtils.stringToList("1,2,3,4,5", ",");
        Assert.assertEquals(5, list.size());
    }
}
