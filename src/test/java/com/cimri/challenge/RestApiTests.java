package com.cimri.challenge;

import com.cimri.challenge.entity.cassandra.ProductExtraInfo;
import com.cimri.challenge.entity.db.Product;
import com.cimri.challenge.service.IProductExtraInfoService;
import com.cimri.challenge.service.IProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestApiTests {

	private final String URL = "/api/";

	@Autowired
	private WebApplicationContext webApplicationContext;
	@MockBean
	private IProductService productService;
	@MockBean
	private IProductExtraInfoService productExtraInfoService;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.build();
	}

	@Test
	public void findProductsByTitle() throws Exception {
		Page<Product> products = new PageImpl<>(new ArrayList<>());
		when(productService
				.findByTitleContaining(anyString(),anyInt(),anyInt()))
				.thenReturn(products);

		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders
						.get(URL + "find-products-by-title?title={title}&start={start}&size={size}",
								anyString(), anyInt(), anyInt())
						.accept(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();
		int status = result.getResponse().getStatus();
		assertEquals("Correct Response Status", HttpStatus.OK.value(), status);
	}

	@Test
	public void findExtrasById() throws Exception {
		List<ProductExtraInfo> productExtraInfoList = new ArrayList<>();
		when(productExtraInfoService
				.findByProductInfoId(anyLong()))
				.thenReturn(productExtraInfoList);

		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders
						.get(URL + "find-extras-by-id?id={id}", anyLong())
						.accept(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();
		int status = result.getResponse().getStatus();
		assertEquals("Correct Response Status", HttpStatus.OK.value(), status);
	}


}
