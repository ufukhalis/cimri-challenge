package com.cimri.challenge;


import com.cimri.challenge.entity.cassandra.ProductExtraInfo;
import com.cimri.challenge.entity.db.Product;
import com.cimri.challenge.entity.db.ProductInfo;
import com.cimri.challenge.service.IProductExtraInfoService;
import com.cimri.challenge.service.IProductInfoService;
import com.cimri.challenge.service.IProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceTests {

    @MockBean
    private IProductService productService;
    @MockBean
    private IProductInfoService productInfoService;
    @MockBean
    private IProductExtraInfoService productExtraInfoService;

    @Mock
    Product product;
    @Mock
    List<Product> products;

    @Mock
    ProductInfo productInfo;
    @Mock
    List<ProductInfo> productInfoList;

    @Mock
    ProductExtraInfo productExtraInfo;
    @Mock
    List<ProductExtraInfo> productExtraInfoList;

    @Test
    public void findByTitle() {
        when(productService
                .findByTitle(anyString()))
                .thenReturn(product);
        Product p = productService.findByTitle(anyString());
        assertNotNull(p);
    }

    @Test
    public void findByTitleContaining() {
        when(productService
                .findByTitleContaining(anyString()))
                .thenReturn(products);
        List<Product> productList = productService.findByTitleContaining(anyString());
        assertNotNull(productList);
    }

    @Test
    public void findById() {
        when(productService.findById(anyLong())).thenReturn(product);
        Product p = productService.findById(anyLong());
        assertNotNull(p);
    }

    @Test
    public void saveProduct() {
        when(productService.save(anyObject())).thenReturn(product);
        Product p = productService.save(anyObject());
        assertNotNull(p);
    }

    @Test
    public void findByProduct() {
        when(productInfoService.findByProduct(anyObject())).thenReturn(productInfoList);
        List<ProductInfo> list = productInfoService.findByProduct(anyObject());
        assertNotNull(list);
    }

    @Test
    public void findBySourceAndProduct() {
        when(productInfoService.findBySourceAndProduct(anyString(), anyObject())).thenReturn(productInfo);
        ProductInfo pi = productInfoService.findBySourceAndProduct(anyString(), anyObject());
        assertNotNull(pi);
    }

    @Test
    public void saveProductInfo() {
        when(productInfoService.save(anyObject())).thenReturn(productInfo);
        ProductInfo pi = productInfoService.save(anyObject());
        assertNotNull(pi);
    }

    @Test
    public void findByProductInfoId() {
        when(productExtraInfoService.findByProductInfoId(anyLong())).thenReturn(productExtraInfoList);
        List<ProductExtraInfo> list = productExtraInfoService.findByProductInfoId(anyLong());
        assertNotNull(list);
    }

    @Test
    public void saveProductExtraInfo() {
        when(productExtraInfoService.save(anyObject())).thenReturn(productExtraInfo);
        ProductExtraInfo pei = productExtraInfoService.save(anyObject());
        assertNotNull(pei);
    }

}
